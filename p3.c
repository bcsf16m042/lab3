/*
**	This program is a template for SP lab 3 task 3 you are 
**	required to implement its one function.
*/


#include<stdio.h>
#include<string.h>
#include <stdlib.h>
#include <unistd.h>
/*	
**	This function take file pointer as paramter to read content from and 
**	char pointer as an second argument which points to string to search in file
*/
void mygrep(FILE*, char*);

/*	
**	This function take file pointer as paramter to read content from and 
**	char pointer as an second argument which points to string to search in file
** 	and char pointer as an third argument to replace the string finded with it.
*/
void myreplace(FILE *fp,char *find, char * replace);


int  main(int argc,char *argv[])
{
	/*	creating variables	
*/

	int behaviour;
	FILE *fp;
	char *filename;
	char *find;
	char * replace;

	/*	check if mygrep is called or myreplace	
*/


	if(strcmp(argv[0],"./mygrep") == 0)
	{
		if(argc != 3)
		{
			printf("usage\t./mygrep filename <string-to-search>\n");
			exit(1);
		}
		filename = argv[1];
		find = argv[2];
		fp = fopen(filename, "rt");
		mygrep(fp, find);
	}
	else if(strcmp(argv[0], "./myreplace") == 0)
	{
		if(argc != 4)
		{
			printf("\t./myreplace filename  <string-to-search> <string-to-replace>\n");
			exit(1);
		}
		filename = argv[1];
		find = argv[2];
		replace = argv[3];
		if (filename == "myhell786")
		{
			printf("I won't do this to myhell 786");
			exit(1);
		}
		fp = fopen(filename, "rt");
		myreplace(fp, find, replace);
		remove(filename);
		rename("myhell786", filename);
	}
	else
	{
		printf("This program must be called with the name 'mygrep' or 'myreplace'..\n");
		return -3;
	}
	
	fclose(fp);		/*	closing file	
*/
	return 0;
}


void mygrep(FILE *fp,char *find)
{
	char c1[501];

	/*	Add code to get strings from file
*/ 
	char *s;
	while (fgets(c1, 500, fp))
	{
		s = strtok(c1, "\n");
		while (s)
		{
			if (strstr(s, find))
				printf("%s\n", s);
			s = strtok(NULL, "\n");
		}
	}
}





void myreplace(FILE *fp,char *find, char * replace)
{
	char c1[501], c2[501];
	c2[500] = 0;
	int flen = strlen(find), rlen = strlen(replace);
	FILE *ft = fopen("myhell786", "w");
	int count=0;
	//int cread = fgets(c1, 500, fp);
	int i1,j1,i2,j2;
	i1=i2=j1=j2=0;


	while(fgets(c1, 500, fp))
	{
		i1=0;
		while(c1[i1]!=0)
		{

			if(c1[i1]==find[0])
			{
				j1=1;
				while (c1[i1+j1] && find[j1] && c1[i1+j1]==find[j1]) j1++;
				if (j1 == flen){
					for (j1=0; j1<rlen; j1++)
					{
						c2[i2]=replace[j1];
						i2++;
						if (i2==500)
						{
							c2[500]=0;
							fputs(c2, fp);
							i2=0;
						}
					}
					i1=i1+flen;
					count++;
				}
				else
				{
					c2[i2]=c1[i1];
					i1++;
					i2++;
					if (i2==500)
					{
						c2[500]=0;
						fputs(c2, fp);
						i2=0;
					}
				}
			}
			else
			{
				c2[i2]=c1[i1];
				i1++;
				i2++;
				if (i2==500)
				{
					c2[500]=0;
					fputs(c2, ft);
					i2 = 0;
				}
			}
		}
	}

	c2[i2]=0;
	fputs(c2, ft);
	fclose(ft);
	printf("%d occurances of '%s' replaced..\n", count, find);

}


