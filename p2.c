#include<stdio.h>
#include<stdlib.h>
int main(int argc, char *argv[])
{
	if (argc!=2)
	{
		printf("Invalid amount of arguments.\n");
		return -1;
	}
	float f=0;
	int i=0, n=0, x=1;
	_Bool neg=0;
	if (argv[1][0]=='-'){
		i++;
		neg = 1;
	}
	while (argv[1][i]!='\0' && argv[1][i]!='.')
	{
		if (argv[1][i]>= '0' && argv[1][i]<= '9')
			n= n*10 + argv[1][i] - '0';
		else
		{
			printf("not a valid floating point number.\n");
			return -2;
		}
		i++;
	}
	f=n;
	n=0;
	if (argv[1][i] == '.')
		i++;
	while (argv[1][i] != '\0')
	{
		if (argv[1][i]>= '0' && argv[1][i]<= '9')
		{
			n= n*10 + argv[1][i] - '0';
			x*=10;
		}
		else
		{
			printf("not a valid floating point number.\n");
			return -2;
		}
		i++;
	}
	f = f + (float)n /x;
	if (neg)
		f = -f;
	printf("you entered %f.\n", f);
	return 0;
}
